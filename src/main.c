#define _DEFAULT_SOURCE

#include <unistd.h>

#include "mem.h"
#include "mem_internals.h"

void* heap;
struct block_header* block;

static int block_count(struct block_header* block) {
  int count = 0;
  struct block_header* iteration = block;
  while(iteration->next) {
  	if (!iteration->is_free) count++;
  	iteration = iteration->next;
  }
  return count;
}

static bool init_env() {
  heap = heap_init(REGION_MIN_SIZE);
  block = (struct block_header*) heap;
  if(!heap || !block)
  	return false;
  return true;
}

static void test_1() {
  printf("First test: allocation of one block\n");
  printf("Heap pre-allocation:\n");
  debug_heap(stdout, heap);
  void* test = _malloc(1024);
  printf("Heap post-allocation:\n");
  debug_heap(stdout, heap);
  if(!test || block->capacity.bytes != 1024) {
  	printf("Memory not allocated on test 1\n");
  	printf("Completed first test.\n\n");
  }
  else {
  	printf("Completed first test.\n\n");
  }
  _free(test);
}

static void test_2() {
  printf("Second test: allocation of several blocks, freeing of one block\n");
  printf("Heap before allocation\n");
  debug_heap(stdout, heap);
  void* test_1 = _malloc(1024);
  void* test_2 = _malloc(2048);
  printf("Heap after allocation\n");
  debug_heap(stdout, heap);
  if(block_count(block) != 2) {
  	printf("Incorrect number of blocks allocated\n");
  	_free(test_1);
  	_free(test_2);
  } else {
  	_free(test_1);
  	printf("Heap after freeing\n");
	debug_heap(stdout, heap);
  	if(block_count(block) == 1)
  		printf("Block freed\n");
  	else printf("Block not freed, left blocks: %d \n", block_count(block));
  	_free(test_2);
  }
  printf("Completed second test.\n\n");
}

static void test_3() {
  printf("Third test: allocation of several blocks, freeing of several blocks\n");
  printf("Heap before allocation\n");
  debug_heap(stdout, heap);
  void* test_1 = _malloc(1024);
  void* test_2 = _malloc(2048);
  void* test_3 = _malloc(4096);
  printf("Heap after allocation\n");
  debug_heap(stdout, heap);
  if(block_count(block) != 3) {
  	printf("Incorrect number of blocks allocated\n");
  	_free(test_1);
  	_free(test_2);
  	_free(test_3);
  } else {
  	_free(test_1);
  	_free(test_2);
  	printf("Heap after freeing\n");
	debug_heap(stdout, heap);
  	if(block_count(block) == 1)
  		printf("Freeing successful.\n");
  	else printf("Blocks not freed, left blocks: %d \n", block_count(block));
  	_free(test_3);
  }
  printf("Completed third test.\n\n");
}

static void test_4() {
  printf("Fourth test: heap growing\n");
  printf("Heap before allocation\n");
  debug_heap(stdout, heap);
  void* test_1 = _malloc(8192);
  void* test_2 = _malloc(4096);
  printf("Heap after allocation\n");
  debug_heap(stdout, heap);
  if(block_count(block) != 2)
  	printf("Heap not grown\n");
  else printf("Heap grown\n");
  _free(test_1);
  _free(test_2);
  printf("Completed fourth test.\n\n");
}

static void test_5() {
    printf("Final test: mapping next block address to last block\n");
    while(block->next)
    	block = block->next;
    void* addr_1 = block + block->capacity.bytes;
    addr_1 = mmap((uint8_t*) (getpagesize() * ((size_t) addr_1 / getpagesize() + (((size_t) addr_1 % getpagesize()) > 0))), 1000, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, 0, 0);
    printf("Mapping done for  %p.\n", addr_1);
	printf("Heap before allocation\n");
    debug_heap(stdout, heap);
    void* test = _malloc(12288);
    printf("Heap after allocation\n");
    debug_heap(stdout, heap);
    _free(test);
    printf("Completed final test.\n\n");
}

int main() {
  if(init_env()) {
  	test_1();
  	test_2();
  	test_3();
  	test_4();
  	test_5();
  	return 0;
  } else printf("Testing environment not initiated\n");
  return -1;
}
